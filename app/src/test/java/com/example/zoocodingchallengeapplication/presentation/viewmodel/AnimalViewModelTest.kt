import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.zoocodingchallengeapplication.MainCoroutineScopeRule
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.domain.usecase.GetAnimalUseCase
import com.example.zoocodingchallengeapplication.domain.usecase.SearchAnimalUseCase
import com.example.zoocodingchallengeapplication.presentation.viewmodel.AnimalViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class AnimalViewModelTest {

    // Rule to swap the background executor used by the Architecture Components with a TestCoroutineDispatcher
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainTaskExecutorRule = MainCoroutineScopeRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var viewModel: AnimalViewModel

    @Mock
    private lateinit var getAnimalUseCase: GetAnimalUseCase

    @Mock
    private lateinit var searchAnimalUseCase: SearchAnimalUseCase

    private val animalResponseItems: ArrayList<AnimalResponseItem> = Mockito.mock()

    @Before
    fun setUp() {
        // Initialize Mockito annotations
        MockitoAnnotations.initMocks(this)
        viewModel = AnimalViewModel(getAnimalUseCase, searchAnimalUseCase)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `getAnimals should update animalsLiveData when animals list is empty`() =
        testDispatcher.runBlockingTest {
            // Arrange
            val animalName = "dog"
            Mockito.`when`(getAnimalUseCase.invoke(animalName))
                .thenReturn(animalResponseItems)

            // Act
            viewModel.getAnimals(animalName)
            testDispatcher.scheduler.advanceUntilIdle()
            // Assert
            assert(viewModel.animalsLiveData.value == animalResponseItems)
        }

    @Test
    fun `test getAnimals() error()`() = testDispatcher.runBlockingTest {
        val errorMessage = "Error fetching data"
        val animalName = "dog"
        Mockito.`when`(getAnimalUseCase.invoke(animalName))
            .thenThrow(RuntimeException(errorMessage))
        // Act
        viewModel.getAnimals(animalName)
        testDispatcher.scheduler.advanceUntilIdle()
        // Assert
        assert(viewModel.animalsLiveData.value == null)
        assert(viewModel.errorLiveData.value == "Something went wrong")
    }

    @Test
    fun `searchAnimals should update animalsLiveData`() = testDispatcher.runBlockingTest {
        // Arrange
        val query = "bull"
        val type = "dog"
        Mockito.`when`(searchAnimalUseCase.invoke(query, type)).thenReturn(animalResponseItems)
        // Act
        viewModel.searchAnimals(query, type)
        // Assert
        assert(viewModel.animalsLiveData.value == animalResponseItems)
    }
}
