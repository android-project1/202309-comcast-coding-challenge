package com.example.zoocodingchallengeapplication.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem

@Database(entities = [AnimalResponseItem::class], version = 1)
@TypeConverters(StringListConverter::class)
abstract class AnimalDatabase : RoomDatabase() {
    abstract fun animalDao(): AnimalDao
}