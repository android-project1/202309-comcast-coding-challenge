package com.example.zoocodingchallengeapplication.data.model

import androidx.room.Entity

@Entity(tableName = "characteristics")
data class Characteristics(
    val age_of_sexual_maturity: String = "",
    val age_of_weaning: String = "",
    val average_clutch_size: String = "",
    val average_litter_size: String = "",
    val biggest_threat: String = "",
    val color: String = "",
    val common_name: String = "",
    val diet: String = "",
    val distinctive_feature: String = "",
    val estimated_population_size: String = "",
    val habitat: String = "",
    val lifespan: String = "",
    val predators: String = "",
    val prey: String = "",
    val slogan: String = "",
    val wingspan: String = ""
)