package com.example.zoocodingchallengeapplication.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem

@Dao
interface AnimalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: ArrayList<AnimalResponseItem>)

    @Query("SELECT * FROM animal_response_items WHERE animalType = :type and (name LIKE '%' || :searchName || '%' or characteristics_common_name LIKE '%' || :searchName || '%') ")
    fun searchAnimalsByName(searchName: String, type: String): List<AnimalResponseItem>

    @Query("SELECT * FROM animal_response_items WHERE animalType = :type")
    fun getAnimalsByType(type: String): List<AnimalResponseItem>

    @Query("DELETE FROM animal_response_items WHERE animalType = :type")
    fun deleteAnimalsByType(type: String)
}