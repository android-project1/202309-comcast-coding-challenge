package com.example.zoocodingchallengeapplication.data.repository

import android.util.Log
import com.example.zoocodingchallengeapplication.data.db.AnimalDao
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.data.service.AnimalsApiService
import com.example.zoocodingchallengeapplication.domain.IAnimalRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AnimalRepository @Inject constructor(
    private val animalsApiService: AnimalsApiService,
    private val animalDao: AnimalDao
) : IAnimalRepository {

    private suspend fun getAnimals(name: String): ArrayList<AnimalResponseItem> {
        // Make a network request to fetch animals from the API using the ApiService
        return animalsApiService.getAnimals(name)
    }

    // Suspend function to insert a list of items
    private suspend fun insertItems(items: ArrayList<AnimalResponseItem>) {
        withContext(Dispatchers.IO) {
            animalDao.insertAll(items)
        }
    }

    private suspend fun getAnimalByType(type: String): ArrayList<AnimalResponseItem> {
        Log.d("AnimalRepository", "get all items by type")
        return withContext(Dispatchers.IO) {
            ArrayList(animalDao.getAnimalsByType(type))
        }
    }

    private suspend fun deleteAnimalByType(type: String) {
        Log.d("AnimalRepository", "delete all animal by type")
        return withContext(Dispatchers.IO) {
            animalDao.deleteAnimalsByType(type)
        }
    }

    override suspend fun searchAnimalsByName(searchString: String, type: String): ArrayList<AnimalResponseItem> {
        return withContext(Dispatchers.IO) {
            ArrayList(animalDao.searchAnimalsByName(searchString, type))
        }
    }

    private val fetchedAnimalsMap = mutableMapOf<String, Long>()

    override suspend fun fetchAndCacheAnimalsIfNeeded(name: String): ArrayList<AnimalResponseItem> {
        val currentTimeMillis = System.currentTimeMillis()
        val shouldCallApi = !fetchedAnimalsMap.containsKey(name) ||
                (currentTimeMillis - fetchedAnimalsMap[name]!! >= TimeUnit.MINUTES.toMillis(10))

        return if (shouldCallApi) {
            Log.d("AnimalRepository", "Fetching fresh data from the API")
            try {
                // Make an API call to fetch fresh data
                val animalDataList = getAnimals(name)
                val animalsWithTypes = animalDataList.map { animal ->
                    animal.copy(animalType = name)
                }
                // Update the cache with the new data and timestamp
                updateOrInsertToDBForCache(ArrayList(animalsWithTypes),name)
                fetchedAnimalsMap[name] = currentTimeMillis

                ArrayList(animalsWithTypes)
            } catch (e: IOException) {
                Log.e("AnimalRepository", "Network error: ${e.message}")
                arrayListOf()
            }
        } else {
            // Use the cached data from the database
            Log.d("AnimalRepository", "Using cached data")
            val cachedAnimal = getAnimalByType(name)
            if (cachedAnimal.isNotEmpty()) {
                ArrayList(cachedAnimal)
            } else {
                arrayListOf()
            }
        }
    }

    private suspend fun updateOrInsertToDBForCache(
        animals: ArrayList<AnimalResponseItem>,
        type: String
    ) {
        try {
            Log.d("AnimalRepository", "Updating cache with fresh data")
            //delete old data and Cache the fetched animals in the local Room database
            deleteAnimalByType(type)
            insertItems(animals)
        } catch (e: Exception) {
            // Handle network request errors here
            Log.d("AnimalRepository", e.localizedMessage)
        }
    }
}