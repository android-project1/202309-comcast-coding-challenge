package com.example.zoocodingchallengeapplication.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.zoocodingchallengeapplication.data.db.StringListConverter

@Entity(tableName = "animal_response_items")
data class AnimalResponseItem(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    @Embedded(prefix = "characteristics_")
    val characteristics: Characteristics,
    @TypeConverters(StringListConverter::class)
    val locations: List<String>,
    val name: String = "",
    @Embedded(prefix = "taxonomy_")
    val taxonomy: Taxonomy,
    val animalType: String = ""
)