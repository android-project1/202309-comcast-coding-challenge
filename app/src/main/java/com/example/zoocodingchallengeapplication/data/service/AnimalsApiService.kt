package com.example.zoocodingchallengeapplication.data.service

import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface AnimalsApiService {
    @GET("animals")
    @Headers("X-Api-Key: hfzU/CRReGRxuNUXfqzuJQ==HSMctzE8woGjhpE4")
    suspend fun getAnimals(@Query("name") name: String): ArrayList<AnimalResponseItem>
}
