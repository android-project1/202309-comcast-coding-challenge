package com.example.zoocodingchallengeapplication.data.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "taxonomy")
data class Taxonomy(
    @SerializedName("class")
    val className: String = "",
    val family: String = "",
    val genus: String = "",
    val kingdom: String = "",
    val order: String = "",
    val phylum: String = "",
    val scientific_name: String = ""
)