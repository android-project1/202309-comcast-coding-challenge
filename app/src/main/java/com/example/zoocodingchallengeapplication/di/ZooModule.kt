package com.example.zoocodingchallengeapplication.di

import android.content.Context
import androidx.room.Room
import com.example.zoocodingchallengeapplication.data.db.AnimalDao
import com.example.zoocodingchallengeapplication.data.db.AnimalDatabase
import com.example.zoocodingchallengeapplication.data.repository.AnimalRepository
import com.example.zoocodingchallengeapplication.data.service.AnimalsApiService
import com.example.zoocodingchallengeapplication.domain.IAnimalRepository
import com.example.zoocodingchallengeapplication.domain.usecase.GetAnimalUseCase
import com.example.zoocodingchallengeapplication.domain.usecase.SearchAnimalUseCase
import com.example.zoocodingchallengeapplication.presentation.viewmodel.AnimalViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ZooModule {

    @Provides
    fun provideAnalyticsService(): AnimalsApiService{
        return Retrofit.Builder()
            .baseUrl("https://api.api-ninjas.com/v1/")
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AnimalsApiService::class.java)

    }

    @Provides
    @Singleton
    fun provideAnimalDatabase(@ApplicationContext context: Context):AnimalDatabase{
        return Room.databaseBuilder(context, AnimalDatabase::class.java, "database_animal_zoo").build()
    }

    @Provides
    fun provideAnimalDao(animalDatabase: AnimalDatabase): AnimalDao{
        return animalDatabase.animalDao()
    }

    @Provides
    fun provideAnimalResponseItemRepository(service: AnimalsApiService,dao: AnimalDao): IAnimalRepository{
        return AnimalRepository(service, dao)
    }

}