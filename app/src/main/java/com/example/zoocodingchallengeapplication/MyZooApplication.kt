package com.example.zoocodingchallengeapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyZooApplication: Application() {
}