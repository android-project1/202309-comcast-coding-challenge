package com.example.zoocodingchallengeapplication.domain

import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem

interface IAnimalRepository {
    suspend fun fetchAndCacheAnimalsIfNeeded(name: String): ArrayList<AnimalResponseItem>
    suspend fun searchAnimalsByName(searchString: String, type: String): ArrayList<AnimalResponseItem>
}