package com.example.zoocodingchallengeapplication.domain.model

//  Rename and change types of parameters in future i will convert data model to this Domain model to display on screen
//  i will use mapper class to convert data to domain model
//  ui(fragment or activity should not be aware about data model as they require only domain model)
//  currently i am not using this but for future reference i kept this class

data class Animal(
    val name: String,
    val phylum: String,
    val scientific_name: String,
    val common_name: String,
    val slogan: String? = null,
    val lifespan: String? = null,
    val wingspan: String? = null,
    val habitat: String? = null,
    val prey: String? = null,
    val predators: String? = null
)

