package com.example.zoocodingchallengeapplication.domain.usecase

import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.domain.IAnimalRepository
import javax.inject.Inject

class GetAnimalUseCase @Inject constructor(private val animalRepository: IAnimalRepository) {
     suspend fun invoke(animalName: String):ArrayList<AnimalResponseItem>{
         return animalRepository.fetchAndCacheAnimalsIfNeeded(animalName)
    }
}