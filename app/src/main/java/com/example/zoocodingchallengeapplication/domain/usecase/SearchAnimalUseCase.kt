package com.example.zoocodingchallengeapplication.domain.usecase

import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.domain.IAnimalRepository
import javax.inject.Inject

class SearchAnimalUseCase @Inject constructor(private val animalRepository: IAnimalRepository) {
    suspend fun invoke(searchAnimalName: String, type: String):ArrayList<AnimalResponseItem>{
        return animalRepository.searchAnimalsByName(searchAnimalName, type)
    }
}