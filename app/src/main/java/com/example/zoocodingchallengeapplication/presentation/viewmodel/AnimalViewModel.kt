package com.example.zoocodingchallengeapplication.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.domain.usecase.GetAnimalUseCase
import com.example.zoocodingchallengeapplication.domain.usecase.SearchAnimalUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AnimalViewModel @Inject constructor(
    private val dogUseCase: GetAnimalUseCase,
    private val searchAnimalUseCase: SearchAnimalUseCase
) : ViewModel() {

    val animalsLiveData = MutableLiveData<ArrayList<AnimalResponseItem>>()
    val errorLiveData = MutableLiveData<String>("")
    fun getAnimals(animalName: String) {
        viewModelScope.launch {
            try {
                val animals =
                    animalsLiveData.value ?: ArrayList() // Use the existing value or an empty list
                if (animals.isEmpty()) {
                    animalsLiveData.postValue(dogUseCase.invoke(animalName))
                }
            } catch (e: Exception) {
                println(e.localizedMessage)
                errorLiveData.postValue("Something went wrong")
            }
        }

    }

    fun searchAnimals(query: String, type: String) {
        viewModelScope.launch {
            try {
                animalsLiveData.postValue(searchAnimalUseCase.invoke(query, type))
            } catch (e: Exception) {

            }
        }
    }

    public override fun onCleared() {
        super.onCleared()
        animalsLiveData.value = ArrayList()
    }
}
