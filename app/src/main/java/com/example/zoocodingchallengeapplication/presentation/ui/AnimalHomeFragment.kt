package com.example.zoocodingchallengeapplication.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.zoocodingchallengeapplication.databinding.FragmentAnimalHomeBinding

/**
 * A simple [Fragment] subclass.
 * Use the [AnimalHomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AnimalHomeFragment : Fragment() {

    private var _binding: FragmentAnimalHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAnimalHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.dogButton.setOnClickListener {
            view.findNavController().navigate(AnimalHomeFragmentDirections.navDetailFragment("dog"))
        }
        binding.birdButton.setOnClickListener {
            view.findNavController()
                .navigate(AnimalHomeFragmentDirections.navDetailFragment("bird"))
        }
        binding.bugButton.setOnClickListener {
            view.findNavController().navigate(AnimalHomeFragmentDirections.navDetailFragment("bug"))
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}