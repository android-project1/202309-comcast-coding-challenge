package com.example.zoocodingchallengeapplication.presentation.ui

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.zoocodingchallengeapplication.data.model.AnimalResponseItem
import com.example.zoocodingchallengeapplication.presentation.viewmodel.AnimalViewModel

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AnimalListScreen(animalName: String) {
    val viewModel: AnimalViewModel = hiltViewModel()
    val animals by viewModel.animalsLiveData.observeAsState(emptyList())

    // Search state
    var searchText by remember { mutableStateOf(TextFieldValue()) }

    // Initialize search query
    LaunchedEffect(animalName) {
        searchText = TextFieldValue("")
    }
    LaunchedEffect(true) {
        viewModel.getAnimals(animalName)
    }

    val isLandscape = LocalConfiguration.current.orientation == Configuration.ORIENTATION_LANDSCAPE

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        // Search bar
        OutlinedTextField(
            value = searchText,
            onValueChange = {
                searchText = it
                viewModel.searchAnimals(it.text, animalName)
            },
            placeholder = {
                Text(text = "Search Animals")
            },
            modifier = Modifier.fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(16.dp))

        // Filtering logic
        val filteredAnimals = animals.filter { animal ->
            animal.name.contains(searchText.text, ignoreCase = true)
        }

        if (isLandscape) {
            // Horizontally scrolling list in landscape mode
            LazyRow(
                modifier = Modifier.fillMaxWidth(),
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
            ) {
                items(filteredAnimals) { animal ->
                    AnimalListItem(animal = animal)
                    Spacer(modifier = Modifier.width(16.dp))
                }
            }
        } else {
            // Vertically scrolling list in portrait mode
            LazyColumn {
                items(filteredAnimals) { animal ->
                    AnimalListItem(animal = animal)
                    Divider()
                }
            }
        }
    }
}


@Composable
fun AnimalListItem(animal: AnimalResponseItem) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 4.dp
        )
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {
            // Animal Name
            Text(
                text = animal.name,
                style = androidx.compose.ui.text.TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier.padding(bottom = 4.dp)
            )

            // Phylum
            if (!animal.taxonomy.phylum.isNullOrBlank()) {
                Text(
                    text = "Phylum: ${animal.taxonomy.phylum}",
                    style = androidx.compose.ui.text.TextStyle(fontSize = 16.sp),
                    modifier = Modifier.padding(bottom = 4.dp)
                )
            }

            // Scientific Name
            if (!animal.taxonomy.scientific_name.isNullOrBlank()) {
                Text(
                    text = "Scientific Name: ${animal.taxonomy.scientific_name}",
                    style = androidx.compose.ui.text.TextStyle(fontSize = 16.sp),
                    modifier = Modifier.padding(bottom = 4.dp)
                )
            }

            // Display additional characteristics based on animal type
            when (animal.animalType) {
                "dog" -> {
                    if (!animal.characteristics.lifespan.isNullOrBlank()) {
                        Text(
                            text = "Lifespan: ${animal.characteristics.lifespan}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                    if (!animal.characteristics.slogan.isNullOrBlank()) {
                        Text(
                            text = "Slogan: ${animal.characteristics.slogan}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                }

                "bird" -> {
                    if (!animal.characteristics.wingspan.isNullOrBlank()) {
                        Text(
                            text = "Wingspan: ${animal.characteristics.wingspan}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                    if (!animal.characteristics.habitat.isNullOrBlank()) {
                        Text(
                            text = "Habitat: ${animal.characteristics.habitat}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                }

                "bug" -> {
                    if (!animal.characteristics.prey.isNullOrBlank()) {
                        Text(
                            text = "Prey: ${animal.characteristics.prey}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                    if (!animal.characteristics.predators.isNullOrBlank()) {
                        Text(
                            text = "Predators: ${animal.characteristics.predators}",
                            style = TextStyle(fontSize = 16.sp),
                            modifier = Modifier.padding(bottom = 4.dp)
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {

    AnimalListScreen(animalName = "dog")

}
