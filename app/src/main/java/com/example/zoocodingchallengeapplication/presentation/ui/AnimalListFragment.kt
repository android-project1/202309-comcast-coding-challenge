package com.example.zoocodingchallengeapplication.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

private const val ANIMAL_NAME = "ANIMAL_NAME"

/**
 * A simple [Fragment] subclass.
 * Use the [AnimalListFragment.newInstance] factory method to
 * create an instance of this fragment.
 * i have not used compose for this fragment to display both the fragment diff way
 * 1 using xml and viewbinding
 * 2nd using compose
 */
@AndroidEntryPoint
class AnimalListFragment :
    Fragment() {

    private var animalName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            animalName = it.getString(ANIMAL_NAME)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent { animalName?.let { AnimalListScreen(animalName = it) } }
        }
    }
}